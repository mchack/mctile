# mctile

A simple layout generator for the
[river](https://github.com/riverwm/river) Wayland compositor.

It's basically `river/contrib/layout.c` with an added monocle layout
and corresponding commands.

Typically you want to bind:

```
# Super+H and Super+L to decrease/increase the main ratio
riverctl map normal Super H send-layout-cmd tile "main_ratio -0.05"
riverctl map normal Super L send-layout-cmd tile "main_ratio +0.05"

# Increase/decrease number of views in main track
riverctl map normal Super I send-layout-cmd tile "main_count +1"
riverctl map normal Super D send-layout-cmd tile "main_count -1"

# Change to monocle mode
riverctl map normal Super M send-layout-cmd tile "monocle"

# Change back to tiling mode
riverctl map normal Super T send-layout-cmd tile "tile"
```
## Building

Just `make` should do it.

I've included generated code from these commands to get the River
layout protocol:

```
% wayland-scanner private-code < ~/github/river/protocol/river-layout-v3.xml > river-layout-v3.c
% wayland-scanner client-header < ~/github/river/protocol/river-layout-v3.xml > river-layout-v3.h
```
