all: mctile

CFLAGS=-Wall -Wextra -Wpedantic -Wno-unused-parameter
LDFLAGS=-lwayland-client

OBJS=mctile.o river-layout-v3.o

mctile: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LDFLAGS) -o $@
